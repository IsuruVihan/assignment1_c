/*
	Created by :- Isuru Harischandra
	Student ID :- 202032 (Temporary)
*/

//This program will get two floating points as inputs and print the multiplication of them
#include <stdio.h>

int main() {
	
	float num1, num2, answer;

	//Get first user input
	printf("Enter your first number : \n");
	scanf("%f", &num1);

	//Get second user input
	printf("Enter your second number : \n");
	scanf("%f", &num2);

	//Calculation
	answer = num1 * num2;

	//Print answer
	printf("Answer : %f\n", answer);

	return 0;

}