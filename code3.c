/*
	Created by :- Isuru Harischandra
	Student ID :- 202032 (Temporary)
*/

//This program will get two integers as user inputs then swap each other
#include <stdio.h>

int main() {
	
	int num1, num2, a;

	//Get number 1
	printf("Enter the first number : \n");
	scanf("%d", &num1);

	//Get number 2
	printf("Enter the second number : \n");
	scanf("%d", &num2);

	//Swapping
	a= num1;
	num1 = num2;
	num2 = a;

	//Print answer
	printf("First number is : %d\n", num1);
	printf("Second number is : %d\n", num2);

	return 0;

}