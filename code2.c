/*
	Created by :- Isuru Harischandra
	Student ID :- 202032 (Temporary)
*/

//This program will get radius of a disk as inputs and print the area of that disk
#include <stdio.h>

int main() {
	
	float const pi = 3.14;
	float radious, area;

	//Get radious
	printf("Enter the radious : \n");
	scanf("%f", &radious);

	//Calculation
	area = pi * (radious * radious);

	//Print answer
	printf("Area of the Disk : %f\n", area);

	return 0;

}